import {acceptHMRUpdate, defineStore} from "pinia";

interface TestRequest{
  subjectId : number
  teacherId : number
  abilityUnitFactorName : string
  abilityUnitName : string
  testType : string
  problemNum : number
  testTime : string
  testName : string
  score : number
  level : number
  dateTest : string
  ncs : string
}

export const useTestStore = defineStore('id', {
  state: () => {
    return {
    }
  },
  getters: {
  },


  actions: {
    setTestInfo(data: TestRequest) {
      $fetch('http://192.168.0.151:8080/v1/subjectStatus/new',{
        method:'POST',
        body : data
      });
      console.log(data)
      console.log('hi')
    }

  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useTestStore, import.meta.hot))
}
