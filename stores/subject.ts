import {acceptHMRUpdate, defineStore} from "pinia";

interface subjectRequest{
  id : number
  teacherId : number
  subjectName : string
  registerHuman :number
}

interface subjectDetail{
  subjectName : string
  dateStart :string
  dateEnd : string
  periodTime :number
  trainingType :string
  classTurn : number
  registerHuman : number
  totalHuman : number
  mainTeacher : string
  ncsLevel : number

}

export const useSubjectStore = defineStore('subject', {
  state: () => ({
    subjectData: [] as subjectRequest[],
    subjectDetail: [] as subjectDetail []

  }),

  actions: {

    async getSubjectList(){
      const {data}: any = await useFetch(
        `http://35.193.177.229:8080/v1/subject/all`, {
          method: 'GET',
        });
      if (data) {
        this.subjectData = data.value.list;
        console.log(data.value.list)
      }
    },

    async getSubjectDetail(){
      const {id} = useRoute().params
      const {data}: any = await useFetch(

        `http://192.168.0.151:8080/v1/adviceManagement/detail/adviceManagementId/${id}`,
        {
          method: 'GET',
        });
      if (data) {
        this.subjectDetail = data.value;
      }
    }
  }

})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useSubjectStore, import.meta.hot))
}
