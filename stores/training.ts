import {acceptHMRUpdate, defineStore} from "pinia";

interface trainingRequest {
  subjectId : number
  dateTraining:string
  registrationNum : number
  attendanceNum : number
  theory: number
  practice : number
  absentWho : string[]
  lateWho : string[]
  earlyLeaveWho : string[]
  trainingContent : string
  etc : string
}


// 데이터의 타입을 지정해줘야함
// auth 스토어

export const useTrainingStore = defineStore('training', {
  state: () => ({
    trainingData: [] as trainingRequest[],


  }),
  getters: {
  },

  actions: {
    async setTraining(data: trainingRequest) {
      const token = useCookie('token');
      console.log(token.value)
      // 배포 스웨거 주소
      await useFetch('http://192.168.0.151:8080/v1/trainingRegister/new',{
        method:'POST',
        body : data,
        headers: {
          'Authorization': `Bearer ${token.value}`
        }
      });
      console.log('hello')
      console.log(data)
    },
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useTrainingStore, import.meta.hot))
}
